package com.webclient.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.webclient.model.User;

@Controller
public class UserMVCController {
	
	@Autowired
	RestTemplate restTemplate = new RestTemplate();
	
	//**********************List of users*******************************
	@RequestMapping("/")
    public ModelAndView listUsers() { 
    	String url="http://localhost:8081/users";
    	List<User> users = restTemplate.getForObject(url, List.class);
    	return new ModelAndView("listusers", "users", users);
    }
	
	//**********************Navigate to Find User Page*******************************
	@RequestMapping(value="/displayUser")
	public String viewUser(){
	
		return "findUser";
	}
	
	//**********************Navigate to Delete User Page*******************************
	@RequestMapping(value="/deleteUser")
	public String deleteUser(){
	
		return "deleteUser";
	}
	
	@RequestMapping(value="/updateName")
	public String updateUserName(){
	
		return "updateUserName";
	}
	
	@RequestMapping(value="/updateEmail")
	public String updateUserEmail(){
	
		return "updateUserEmail";
	}
	
	@RequestMapping(value="/updatePassword")
	public String updateUserPassword(){
	
		return "updateUserPassword";
	}
	
	//**********************Go to Add User Page*******************************
	@RequestMapping(value="/registerPage", method = RequestMethod.GET)
	public String registerPage(Model model){
		model.addAttribute("user", new User());
		return "register";
	}
 

	
	//**********************Create new user*******************************
	@RequestMapping(value = "createUser", method = RequestMethod.POST)
	public String createUser(@ModelAttribute ("user") @Valid User user, BindingResult result){
		 
		if (result.hasErrors()) {
			return "register";
	    }
	 	
		String CreateUserUrl = "http://localhost:8081/newUser";
		
		restTemplate.postForObject(CreateUserUrl, user, User.class);
	
		return "regSuccess";
		
	}   
	
	//**********************Review detailed user information*******************************
	@RequestMapping(value = "findUserPage/{email}/x")
  	public ModelAndView viewDetailedUserInfo(@PathVariable String email){
      	String findUserUrl ="http://localhost:8081/users/findByEmail/" + email + ".com";
  		User user = restTemplate.getForObject(findUserUrl, User.class);
  		return new ModelAndView("userFound", "user", user);
  	}
	
	//**********************Delete user*******************************
	@RequestMapping(value = "deleteUserPage/{email}/x")
	public ModelAndView deleteUser(@PathVariable String email){
		String deleteUserUrl = "http://localhost:8081/users/deleteUser/" + email + ".com" ;
		User user = restTemplate.getForObject(deleteUserUrl, User.class);
		
		if(user.getEmail().equals("good")){
			return new ModelAndView("deleteSuccess");
		}
		else if(user.getEmail().equals("doesntExist"))
		{
			return new ModelAndView("deleteUserNotFound");
		}
		else
		{
			return new ModelAndView("deleteFail");
		}
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
}



