package com.webclient.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.webclient.model.User;

@RestController
public class UserRestController {
    
	//************************Review detailed user information************************** 
	
	@RequestMapping(value = "updateUserEmail")
	public String updateUserEmail(@RequestParam ("oldEmail") String oldEmail, @RequestParam ("newEmail")String newEmail){
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put("http://localhost:8081/users/updateEmail/" + oldEmail + "/" + newEmail + ".com", User.class);
		
		return "Email updated";
	}
	
	@RequestMapping(value = "updateUserName")
	public String updateUserName(@RequestParam ("email") String email, @RequestParam ("name") String newName){
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put("http://localhost:8081/users/updateName/" + email + "/" + newName, User.class);
		return "Name updated";
	}
	
	@RequestMapping(value = "updateUserPassword")
	public String updateUserPassword(@RequestParam ("email") String email, @RequestParam ("oldPassword") String oldPassword,  @RequestParam ("newPassword1") String newPassword1, @RequestParam ("nesPassword2") String newPassword2){
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getForObject("users/updatePassword/email/{oldPassword}/{newPassword1}/{newPassword2}", User.class);
		return "Password updated";
	}
	
}
