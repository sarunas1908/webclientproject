package com.webclient.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class User {
	
	@NotEmpty
	@Size(min=3, max = 30)
	private String name;
	
	@NotEmpty
    @Size(min = 7, max = 30)
	private String password;
	
	@NotEmpty
	@Size(min = 8, max = 30)
    @Email
	private String email;
	
	public User(Integer userId, String name, String password, String email) {
		super();
		this.name = name;
		this.password = password;
		this.email = email;
	}
	public User() {
	}

	public String getName() {
		return name;
	}
	public String getPassword() {
		return password;
	}
	public String getEmail() {
		return email;
	} 

	public void setName(String name) {
		this.name = name;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
    public String toString()
    {
        return "{email = "+email+", name = "+name+", password = "+password+"]";
    }
}