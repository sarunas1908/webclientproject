<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Users</title>
</head>
<body>
	<nav class="navbar navbar-inverse">
 		<div class="container-fluid">
 			<div class="navbar-header">
      			<a class="navbar-brand" href="#">User Management</a>
    		</div>
 		<ul class="nav navbar-nav">
 			<li class="active"><a href="/WebClientProject/">User list</a></li>
			<li><a href="/WebClientProject/registerPage">Create user</a></li>
			<li class="dropdown">
        		<a class="dropdown-toggle" data-toggle="dropdown" href="#">Update user info
        		<span class="caret"></span></a>
       			<ul class="dropdown-menu">
          			<li><a href="/WebClientProject/updateName">Update name</a></li>
          			<li><a href="/WebClientProject/updateEmail">Update email</a></li>
          			<li><a href="/WebClientProject/updatePassword">Update password</a></li>
        		</ul>
      		</li>
		</ul>
		</div>
	</nav>
<h1 align="center">List of users</h1>
<table class="table table-bordred table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
		</tr>
		</thead>
		<tbody>
			<c:forEach var="users" items="${users}" >
                <tr>
					<td><c:out value="${users.name}" /></td>
					<td><c:out value="${users.email}" /></td>
    				<td><a href="/WebClientProject/deleteUserPage/${users.email}/x" class="btn btn-primary a-btn-slide-text">
       				 <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        			<span><strong>Edit</strong></span>            
    				</a></td>
   					<td><button type="button" class="btn btn-primary a-btn-slide-text" data-toggle="modal" data-target="#mymodal2">
        			<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        			<span><strong>View detailed info</strong></span>  
        			</button>       
    				<div class="modal fade" id="mymodal2">
   						<div class="modal-dialog">
   							<div class="modal-content">
   								<div class="modal-header">
                					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                					<h4 class="modal-title">User information</h4>
           					 	</div>
   								<div class="modal-body">
   									<div class="container-fluid well span6">
										<div class="row-fluid">
        									<div class="span2" >
		    									<img src="#" class="img-circle">
		     								</div>
        									<div class="span8">
            									<h3>User Name:
            									${users.name}</h3>
            									<h6>Email: 
            									${users.email}</h6>
            									<h6><a href="#">More... </a></h6>
        									</div>
										</div>
									</div>
   								</div>
   							</div>
   						</div>
   					</div>
    				</td>
    				<td><a type="button" class="btn btn-primary a-btn-slide-text" data-toggle="modal" data-target="#mymodal">
       				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
        			<span><strong>Delete</strong></span>            
   					</a>
   					<div id="mymodal" class="modal fade">
   						<div class="modal-dialog">
   							<div class="modal-content">
   							 <div class="modal-content">
            				<div class="modal-header">
                			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                			<h4 class="modal-title">Confirm Delete</h4>
           					 </div>
   								<div class="modal-body">
   									Are you sure you want to delete this user? 
   								</div>
   								<div class=modal-footer>
   									<a href="/WebClientProject/deleteUserPage/${users.email}/x" class="btn btn-primary a-btn-slide-text">
        							<span><strong>Delete</strong></span>            
   									</a>
   									<a href="/WebClientProject/" class="btn btn-primary a-btn-slide-text">
   									<span><strong>Cancel</strong></span>    
   									</a>
   								</div>
   							</div>
   						</div>
   					</div>
   					</div>
   					</td>
				</tr>
            </c:forEach>
		</tbody>
	</table> 
	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src='<c:url value="/web-resources/js/js-for-listBooks.js"/>'></script>
</body>
</html>