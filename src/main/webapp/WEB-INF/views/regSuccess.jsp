<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2 align="center">Registration is successful! Welcome</h2>
	<table align="center">
        <tr>
            <td>Email of created user :</td>
            <td>${user.email}</td>
        </tr>
    </table>
    <div align="center">
    	<a href="/WebClientProject/">Home page</a>
    </div>
	
</body>
</html>