<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add User</title>
<style type="text/css">
.error {
        color: red; font-weight: bold;
    }
</style>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
 			<div class="navbar-header">
      			<a class="navbar-brand" href="#">User Management</a>
    		</div>
 		<ul class="nav navbar-nav">
 			<li><a href="/WebClientProject/">User list</a></li>
			<li class="active"><a href="/WebClientProjectregisterPage">Create user</a></li>
			<li class="dropdown">
        		<a class="dropdown-toggle" data-toggle="dropdown" href="#">Update user info
        		<span class="caret"></span></a>
       			<ul class="dropdown-menu">
          			<li><a href="/WebClientProject/updateName">Update name</a></li>
          			<li><a href="/WebClientProject/updateEmail">Update email</a></li>
          			<li><a href="/WebClientProject/updatePassword">Update password</a></li>
        		</ul>
      		</li>
		</ul>
		</div>
	</nav>
	
	<h3 align="center">Fill in the fields:</h3>
  	<form:form align="center" class="form-horizontal" action="/WebClientProject/createUser" method="post" modelAttribute="user">
  		<fieldset>
  			<div class="control-group">
  			 <label class="control-label"  for="name">Name</label>
  			 	<div class="controls">
					<form:input path="name"/><br>
					<form:errors path="name" cssClass="error" />
				</div>
			</div>
			
			
			<div class="control-group">
  			 <label class="control-label"  for="password">Password</label>
  			 	<div class="controls">
				<form:password path="password"/><br>
				<form:errors path="password" cssClass="error" />
				</div>
			</div>
			
			<div class="control-group">
  			 <label class="control-label"  for="password">Email</label>
  			 	<div class="controls">
				<form:input path="email"/><br>
				<form:errors path="email" cssClass="error" />
				</div>
			</div>
			<br>
   
   			<div class="control-group">
      		<!-- Button -->
      			<div class="controls">
    				<input class="btn btn-primary" type="submit" value="Register">
    			</div>
    		</div>
		</fieldset>
 	</form:form>
	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
</body>
</html>