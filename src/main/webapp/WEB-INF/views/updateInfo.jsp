<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<nav class="navbar navbar-default">
 		<div class="container-fluid">
 			<div class="navbar-header">
      			<a class="navbar-brand" href="#">User Services</a>
    		</div>
 		<ul class="nav navbar-nav">
 			<li><a href="/WebClientProject/">Home</a></li>
			<li><a href="/WebClientProject/registerPage">Add user</a></li>
			<li><a href="/WebClientProject/deleteUser">Delete user</a></li>
			<li><a href="/WebClientProject/displayUser">View detailed user information</a></li>
			<li class="active"><a href="/WebClientProject/updateInfo">Update user info</a></li>
		</ul>
		</div>
	</nav>
	<h1 align="center">Update user info</h1>
 	<div align="left">
 		<ul>
		<li><a href="/WebClientProject/updateName">Update user name</a></li>
		<li><a href="/WebClientProject/updateEmail">Update user email</a></li>
		<li><a href="/WebClientProject/updatePassword">Update user password</a></li>
		</ul>
	</div>
	<div align="center">
    	<a href="/WebClientProject/">Home page</a>
    </div>
    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
</body>
</html>