<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<nav class="navbar navbar-inverse">
 		<div class="container-fluid">
 			<div class="navbar-header">
      			<a class="navbar-brand" href="#">User Services</a>
    		</div>
 		<ul class="nav navbar-nav">
 			<li><a href="/WebClientProject/">Home</a></li>
			<li><a href="/WebClientProject/registerPage">Add user</a></li>
			<li><a href="/WebClientProject/listall">Display user list</a></li>
			<li><a href="/WebClientProject/deleteUser">Delete user</a></li>
			<li><a href="/WebClientProject/displayUser">View detailed user information</a></li>
			<li class="dropdown">
        		<a class="dropdown-toggle" data-toggle="dropdown" href="#">Update user info
        		<span class="caret"></span></a>
       			<ul class="dropdown-menu">
          			<li class="active"><a href="/WebClientProject/updateName">Update name</a></li>
          			<li><a href="/WebClientProject/updateEmail">Update email</a></li>
          			<li><a href="/WebClientProject/updateUserPassword">Update password</a></li>
        		</ul>
      		</li>
		</ul>
		</div>
	</nav>
	<div align="center">
		<form:form action="/WebClientProject/updateUserName">
			Email:<input name="email"/><br>
			Name: <input name="name"/><br>
        	<input type="submit" value="Update"/> 
		</form:form>
	</div>
	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
</body>
</html>